# Calculo de Ratings

Pacote responsável por calcular o AD Ratings

Exemplo 01: Simples do calculo:
```injectablephp
use AutoAction\AdRatings\Core\CalculateAdRatings;
use AutoAction\AdRatings\Dto\ToCalculateDto;

// Carregar os DTO com os dados de calculo
$dto = new ToCalculateDto({AMOUNT}, {POINT_PER_ITEM}, {MAX_POINT});

// Executar o calculo do Ratings
$calculate = new CalculateAdRatings($dto);
echo $calculate->getCalculation();
```

Exemplo02: Implementação buscando os valores de configuração de um banco de dados

```injectablephp
use AutoAction\AdRatings\Core\AdRatingsComposite;
use AutoAction\AdRatings\Core\DataMemory;
use AutoAction\AdRatings\Enum\GroupsEnum;

// Dados de configuração:
// 1. Criar uma classe que irá buscar no banco de dados ou qualquer outro lugar as configurações
// 2. Esta classe deverá implementar 'AutoAction\AdRatings\Core\ConfigDataInterface'
// 3. O primeiro parâmetro da classe deve ser o grupo e o segundo a quantidade de itens
$data = new DataMemory(GroupsEnum::PHOTOS, 15);

// Injetar as configurações no Composite e executar o composite
$composite = new AdRatingsComposite($data);
$composite->execute();

// Recuperar o calculo
$calculate = $composite->getCalculateAdRatings();

// Recuperar o valor calculado
echo $calculate->getCalculation();
```

# FAZ A VALIDAÇÃO DO DISPARO AUTOMÁTICO DE COTAÇÃO COM BASE NO RATINGS

```injectablephp
use AutoAction\AdRatings\Core\AdRatingsShootLimitClient;
use AutoAction\AdRatings\Core\HasConfigAdRatings;
use AutoAction\AdRatings\Dto\InputRatingsDto;
use AutoAction\AdRatings\Dto\InputRatingShootLimitDto;
use AutoAction\AdRatings\Enum\KindsEnum;
use AutoAction\AdRatings\Exception\ShootLimitDisabledException;

// buscar as configurações iniciais
$hasConfig = new HasConfigAdRatings(KindsEnum::KIND_CAR);
$hasConfig->setHasCar(true);
$hasConfig->setHasHeavy(true);
$hasConfig->setHasMotorcycle(true);

// inicializa client
$client = new AdRatingsShootLimitClient($this->getConfig());

// carregar dados da avaliação
$client->addInputRating(new InputRatingsDto(1, 1.0));
$client->addInputRating(new InputRatingsDto(2, 4.0));
$client->addInputRating(new InputRatingsDto(3, 3.5));
$client->addInputRating(new InputRatingsDto(4, 1.5));

// carregar dados de limite do esquema de configuração
$client->addInputRatingShootLimit(new InputRatingShootLimitDto(1, 1.0));
$client->addInputRatingShootLimit(new InputRatingShootLimitDto(2, 4.0));

// executar a verificação
$client->execute();

// verificar se o rating é válido
echo $client->isValid();
```