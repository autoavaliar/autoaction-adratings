<?php

declare(strict_types=1);

namespace AutoAction\AdRatings\Enum;

class GroupsEnum
{
    const OPTIONALS = 1;
    const PHOTOS = 2;
    const ITENS = 3;
    const COMMENTS = 4;

    public static function getValidGroups(): array
    {
        return [
            self::OPTIONALS,
            self::PHOTOS,
            self::ITENS,
            self::COMMENTS
        ];
    }
}