<?php

declare(strict_types=1);

namespace AutoAction\AdRatings\Enum;

class KindsEnum
{
    const KINDS = [1, 2, 3, 4, 5, 10, 11, 12, 13, 14];
    const KIND_HEAVY = 1;
    const KIND_MOTORCYCLE = 2;
    const KIND_CAR = 3;
    const KIND_UTILITY = 4;
}