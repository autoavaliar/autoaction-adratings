<?php

declare(strict_types=1);

namespace AutoAction\AdRatings\Dto;

class InputRatingsDto
{
    /** @var int */
    private $groupId;
    /** @var float */
    private $points;

    public function __construct(int $groupId, float $points)
    {
        $this->groupId = $groupId;
        $this->points = $points;
    }

    public function getGroupId(): int
    {
        return $this->groupId;
    }

    public function getPoints(): float
    {
        return $this->points;
    }
}