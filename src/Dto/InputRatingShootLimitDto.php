<?php

declare(strict_types=1);

namespace AutoAction\AdRatings\Dto;

class InputRatingShootLimitDto
{
    /** @var int */
    private $groupId;
    /** @var float */
    private $pointsLimit;

    public function __construct(int $groupId, float $pointsLimit)
    {
        $this->groupId = $groupId;
        $this->pointsLimit = $pointsLimit;
    }

    public function getGroupId(): int
    {
        return $this->groupId;
    }

    public function getPointsLimit(): float
    {
        return $this->pointsLimit;
    }
}