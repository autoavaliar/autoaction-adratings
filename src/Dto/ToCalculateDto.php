<?php

declare(strict_types=1);

namespace AutoAction\AdRatings\Dto;

class ToCalculateDto
{
    private $amount;
    private $pointsPerItem;
    private $maxPoints;

    public function __construct(int $amount, float $pointsPerItem, float $maxPoints)
    {
        $this->amount = $amount;
        $this->pointsPerItem = $pointsPerItem;
        $this->maxPoints = $maxPoints;
    }

    public function getAmount(): int
    {
        return $this->amount;
    }

    public function getPointsPerItem(): float
    {
        return $this->pointsPerItem;
    }

    public function getMaxPoints(): float
    {
        return $this->maxPoints;
    }
}