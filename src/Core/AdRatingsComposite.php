<?php

declare(strict_types=1);

namespace AutoAction\AdRatings\Core;

use AutoAction\AdRatings\Dto\ToCalculateDto;

class AdRatingsComposite
{
    /** @var array */
    private $data;
    /** @var CalculateAdRatings */
    private $calculate;

    public function __construct(ConfigDataInterface $data)
    {
        $this->data = $data;
    }

    public function execute()
    {
        $this->data->execute();
        $dto = new ToCalculateDto(
            $this->data->getAmount(),
            $this->data->getPointsPerItem(),
            $this->data->getMaxPoints()
        );
        $this->calculate = new CalculateAdRatings($dto);
    }

    public function getCalculateAdRatings(): CalculateAdRatings
    {
        return $this->calculate;
    }
}