<?php

declare(strict_types=1);

namespace AutoAction\AdRatings\Core;

use AutoAction\AdRatings\Enum\KindsEnum;
use AutoAction\AdRatings\Exception\KindInvalidException;

class HasConfigAdRatings
{
    /** @var int */
    private $kindId;

    /** @var bool */
    private $hasCar = false;
    /** @var bool */
    private $hasHeavy = false;
    /** @var bool */
    private $hasMotorcycle = false;

    /**
     * @throws KindInvalidException
     */
    public function __construct(int $kindId)
    {
        if (!array_key_exists($kindId, KindsEnum::KINDS)) {
            throw new KindInvalidException('Kind #' . $kindId . ' invalid!');
        }
        $this->kindId = $kindId;
    }

    public function setHasCar(bool $hasCar): HasConfigAdRatings
    {
        $this->hasCar = $hasCar;
        return $this;
    }

    public function setHasHeavy(bool $hasHeavy): HasConfigAdRatings
    {
        $this->hasHeavy = $hasHeavy;
        return $this;
    }

    public function setHasMotorcycle(bool $hasMotorcycle): HasConfigAdRatings
    {
        $this->hasMotorcycle = $hasMotorcycle;
        return $this;
    }

    public function getKindId(): int
    {
        return $this->kindId;
    }

    public function isHasConfig(): bool
    {
        switch ($this->kindId) {
            case KindsEnum::KIND_CAR:
            case KindsEnum::KIND_UTILITY:
                return $this->hasCar;
            case KindsEnum::KIND_MOTORCYCLE:
                return $this->hasMotorcycle;
            case KindsEnum::KIND_HEAVY:
                return $this->hasHeavy;
        }
        return false;
    }
}