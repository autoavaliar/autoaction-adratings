<?php

declare(strict_types=1);

namespace AutoAction\AdRatings\Core;

use AutoAction\AdRatings\Dto\ToCalculateDto;

class CalculateAdRatings
{
    /** @var ToCalculateDto */
    private $dto;

    public function __construct(ToCalculateDto $dto)
    {
        $this->dto = $dto;
    }

    public function getCalculation(): float
    {
        $calculate = $this->dto->getAmount() * $this->dto->getPointsPerItem();
        if ($calculate > $this->dto->getMaxPoints()) {
            return $this->dto->getMaxPoints();
        }
        return $calculate;
    }
}