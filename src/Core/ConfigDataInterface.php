<?php

declare(strict_types=1);

namespace AutoAction\AdRatings\Core;

interface ConfigDataInterface
{
    public function execute();

    public function getGroupId(): int;

    public function getAmount(): int;

    public function getMaxPoints(): float;

    public function getPointsPerItem(): float;

    public function getMaxOfItems(): float;
}