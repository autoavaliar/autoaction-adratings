<?php

declare(strict_types=1);

namespace AutoAction\AdRatings\Core;

use AutoAction\AdRatings\Dto\InputRatingsDto;
use AutoAction\AdRatings\Dto\InputRatingShootLimitDto;
use AutoAction\AdRatings\Exception\ShootLimitDisabledException;

class AdRatingsShootLimitClient
{
    /** @var InputRatingsDto */
    private $inputRatings;
    /** @var InputRatingShootLimitDto */
    private $inputRatingsShootLimit = [];
    /** @var bool */
    private $isValid = true;
    /** @var HasConfigAdRatings */
    private $hasConfig;

    public function __construct(HasConfigAdRatings $hasConfig)
    {
        $this->hasConfig = $hasConfig;
    }

    public function addInputRating(InputRatingsDto $input)
    {
        $this->inputRatings[$input->getGroupId()] = $input;
    }

    public function addInputRatingShootLimit(InputRatingShootLimitDto $input)
    {
        $this->inputRatingsShootLimit[$input->getGroupId()] = $input;
    }

    /**
     * @throws ShootLimitDisabledException
     */
    public function execute()
    {
        if (!$this->hasConfig->isHasConfig()) {
            throw new ShootLimitDisabledException();
        }

        /** @var InputRatingShootLimitDto $item */
        foreach ($this->inputRatingsShootLimit as $item) {
            if (
                !isset($this->inputRatings[$item->getGroupId()]) ||
                (isset($this->inputRatings[$item->getGroupId()]) &&
                    $this->inputRatings[$item->getGroupId()]->getPoints() < $item->getPointsLimit())
            ) {
                $this->isValid = false;
                break;
            }
        }
    }

    public function isValid(): bool
    {
        return $this->isValid;
    }
}