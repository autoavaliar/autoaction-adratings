<?php

declare(strict_types=1);

namespace AutoAction\AdRatings\Core;

use AutoAction\AdRatings\Enum\GroupsEnum;
use InvalidArgumentException;

class DataMemory implements ConfigDataInterface
{
    /** @var array */
    private $data;
    /** @var int */
    private $groupId;
    /** @var int */
    private $amount;
    /** @var int */
    private $kindId;

    public function __construct(int $groupId, int $amount, int $kindId = 3)
    {
        if (!in_array($groupId, GroupsEnum::getValidGroups())) {
            throw new InvalidArgumentException('Invalid group!');
        }
        $this->groupId = $groupId;
        $this->amount = $amount;
    }

    public function execute()
    {
        $this->data = [
            'group_id' => $this->groupId,
            'max_points' => '1.5',
            'points_per_item' => '0.1',
            'max_of_items' => '15',
        ];
    }

    public function getMaxPoints(): float
    {
        return floatval($this->data['max_points']);
    }

    public function getPointsPerItem(): float
    {
        return floatval($this->data['points_per_item']);
    }

    public function getMaxOfItems(): float
    {
        return floatval($this->data['max_of_items']);
    }

    public function getGroupId(): int
    {
        return intval($this->data['group_id']);
    }

    public function getAmount(): int
    {
        return $this->amount;
    }
}