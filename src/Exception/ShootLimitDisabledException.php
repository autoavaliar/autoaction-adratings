<?php

declare(strict_types=1);

namespace AutoAction\AdRatings\Exception;

use Exception;

class ShootLimitDisabledException extends Exception
{
    public function __construct(
        $message = "Functionality disabled for this type of vehicle.",
        $code = 0,
        Throwable $previous = null
    ) {
        parent::__construct($message, $code, $previous);
    }

}