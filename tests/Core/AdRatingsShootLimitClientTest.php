<?php

namespace AutoAction\Tests\AdRatings\Core;

use AutoAction\AdRatings\Core\AdRatingsShootLimitClient;
use AutoAction\AdRatings\Core\HasConfigAdRatings;
use AutoAction\AdRatings\Dto\InputRatingsDto;
use AutoAction\AdRatings\Dto\InputRatingShootLimitDto;
use AutoAction\AdRatings\Enum\KindsEnum;
use AutoAction\AdRatings\Exception\ShootLimitDisabledException;
use PHPUnit\Framework\TestCase;

class AdRatingsShootLimitClientTest extends TestCase
{
    public function getConfig(): HasConfigAdRatings
    {
        $hasConfig = new HasConfigAdRatings(KindsEnum::KIND_CAR);
        $hasConfig->setHasCar(true);
        $hasConfig->setHasHeavy(true);
        $hasConfig->setHasMotorcycle(true);
        return $hasConfig;
    }

    /**
     * @throws ShootLimitDisabledException
     */
    public function testValidationTrue()
    {
        // buscar as configurações iniciais
        $client = new AdRatingsShootLimitClient($this->getConfig());

        // carregar dados da avaliação
        $client->addInputRating(new InputRatingsDto(1, 1.0));
        $client->addInputRating(new InputRatingsDto(2, 4.0));
        $client->addInputRating(new InputRatingsDto(3, 3.5));
        $client->addInputRating(new InputRatingsDto(4, 1.5));

        // carregar dados de limite do esquema de configuração
        $client->addInputRatingShootLimit(new InputRatingShootLimitDto(1, 1.0));
        $client->addInputRatingShootLimit(new InputRatingShootLimitDto(2, 4.0));

        // executar a verificação
        $client->execute();

        // verificar se o rating é válido
        $this->assertSame(true, $client->isValid());
    }

    /**
     * @throws ShootLimitDisabledException
     */
    public function testValidationFalse()
    {
        $client = new AdRatingsShootLimitClient($this->getConfig());
        $client->addInputRating(new InputRatingsDto(1, 1.0));
        $client->addInputRating(new InputRatingsDto(2, 3.0)); // limit 4.0
        $client->addInputRating(new InputRatingsDto(3, 3.5)); // limit 3.5
        $client->addInputRating(new InputRatingsDto(4, 1.5));
        $client->addInputRatingShootLimit(new InputRatingShootLimitDto(1, 1.0));
        $client->addInputRatingShootLimit(new InputRatingShootLimitDto(2, 4.0));
        $client->execute();
        $this->assertSame(false, $client->isValid());
    }

    /**
     * @throws ShootLimitDisabledException
     */
    public function testValidationTrueInvalid()
    {
        $client = new AdRatingsShootLimitClient($this->getConfig());
        $client->addInputRating(new InputRatingsDto(1, 1.0));
        $client->addInputRating(new InputRatingsDto(2, 4.0)); // limit 4.0
        $client->addInputRating(new InputRatingsDto(3, 0)); // limit 3.5
        $client->addInputRating(new InputRatingsDto(4, 1.5));
        $client->addInputRatingShootLimit(new InputRatingShootLimitDto(1, 1.0));
        $client->addInputRatingShootLimit(new InputRatingShootLimitDto(2, 4.0));
        $client->execute();
        $this->assertSame(true, $client->isValid());
    }

    /**
     * @throws ShootLimitDisabledException
     */
    public function testValidationTrueNonexistent()
    {
        $client = new AdRatingsShootLimitClient($this->getConfig());
        //$client->addInputRating(new InputRatingsDto(1, 1.0));
        $client->addInputRating(new InputRatingsDto(2, 4.0)); // limit 4.0
        $client->addInputRating(new InputRatingsDto(3, 0)); // limit 3.5
        $client->addInputRating(new InputRatingsDto(4, 1.5));
        $client->addInputRatingShootLimit(new InputRatingShootLimitDto(1, 1.0));
        $client->addInputRatingShootLimit(new InputRatingShootLimitDto(2, 4.0));
        $client->execute();
        $this->assertSame(false, $client->isValid());
    }

    /**
     * @throws ShootLimitDisabledException
     */
    public function testValidationKindDisabled()
    {
        parent::expectException(ShootLimitDisabledException::class);
        $client = new AdRatingsShootLimitClient(new HasConfigAdRatings(KindsEnum::KIND_CAR));
        $client->execute();
        $this->assertSame(false, $client->isValid());
    }

    public function testValidationRatingShootLimitDisabled()
    {
        $client = new AdRatingsShootLimitClient($this->getConfig());
        $client->addInputRating(new InputRatingsDto(2, 4.0)); // limit 4.0
        $client->execute();
        $this->assertSame(true, $client->isValid());
    }
}
