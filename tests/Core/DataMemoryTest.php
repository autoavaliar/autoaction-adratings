<?php

namespace AutoAction\Tests\AdRatings\Core;

use AutoAction\AdRatings\Core\DataMemory;
use AutoAction\AdRatings\Enum\GroupsEnum;
use InvalidArgumentException;
use PHPUnit\Framework\TestCase;

class DataMemoryTest extends TestCase
{
    public function testInvalidGroup()
    {
        $this->expectException(InvalidArgumentException::class);
        new DataMemory(50, 10);
    }

    public function testValidGroup()
    {
        new DataMemory(GroupsEnum::PHOTOS, 10);
    }
}
