<?php

namespace AutoAction\Tests\AdRatings\Core;

use AutoAction\AdRatings\Core\HasConfigAdRatings;
use AutoAction\AdRatings\Enum\KindsEnum;
use AutoAction\AdRatings\Exception\KindInvalidException;
use PHPUnit\Framework\TestCase;

class HasConfigAdRatingsTest extends TestCase
{
    public function testSetCar()
    {
        $car = new HasConfigAdRatings(KindsEnum::KIND_CAR);
        $car->setHasCar(true);
        $car->setHasHeavy(true);
        $car->setHasMotorcycle(true);
        parent::assertSame(true, $car->isHasConfig());
    }

    public function testSetUtility()
    {
        $car = new HasConfigAdRatings(KindsEnum::KIND_UTILITY);
        $car->setHasCar(true);
        $car->setHasHeavy(true);
        $car->setHasMotorcycle(true);
        parent::assertSame(true, $car->isHasConfig());
    }

    public function testSetHeavy()
    {
        $car = new HasConfigAdRatings(KindsEnum::KIND_HEAVY);
        $car->setHasCar(true);
        $car->setHasHeavy(true);
        $car->setHasMotorcycle(true);
        parent::assertSame(true, $car->isHasConfig());
    }

    public function testSetMotorcycle()
    {
        $car = new HasConfigAdRatings(KindsEnum::KIND_MOTORCYCLE);
        $car->setHasCar(true);
        $car->setHasHeavy(true);
        $car->setHasMotorcycle(true);
        parent::assertSame(true, $car->isHasConfig());
    }

    public function testSetInvalid()
    {
        parent::expectException(KindInvalidException::class);
        $car = new HasConfigAdRatings(200);
        $car->setHasCar(true);
        $car->setHasHeavy(true);
        $car->setHasMotorcycle(true);
        parent::assertSame(true, $car->isHasConfig());
        parent::assertSame(true, $car->isHasConfig());
    }
}
