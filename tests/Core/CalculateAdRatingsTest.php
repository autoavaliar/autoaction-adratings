<?php

namespace AutoAction\Tests\AdRatings\Core;

use AutoAction\AdRatings\Core\CalculateAdRatings;
use AutoAction\AdRatings\Dto\ToCalculateDto;
use PHPUnit\Framework\TestCase;

class CalculateAdRatingsTest extends TestCase
{
    /**
     * @dataProvider providerCalculate
     *
     * @param $amount        int Quantidade de itens
     * @param $pointsPerItem float Quantidade de pontos por Item
     * @param $maxPoint      float Máximo de pontos por cálculo
     * @param $expected      float Resultado esperado
     *
     * @return void
     */
    public function testCalculate(int $amount, float $pointsPerItem, float $maxPoint, float $expected)
    {
        $dto = new ToCalculateDto($amount, $pointsPerItem, $maxPoint);
        $calculate = new CalculateAdRatings($dto);
        $this->assertSame($expected, $calculate->getCalculation());
    }

    public function providerCalculate(): array
    {
        /** [{AMOUNT}, {POINTS_PER_ITEM}, {MAX_POINT}, {EXPECTED}] */
        return [
            [7, 0, 0, 0.0],
            [8, 0.1, 1.5, 0.8],
            [16, 2.5, 4, 4],
            [20, 2.5, 4, 4],
            [30, 2.5, 4, 4],
            [7, 0.2, 1, 1],
        ];
    }
}
