<?php

namespace AutoAction\Tests\AdRatings\Core;

use AutoAction\AdRatings\Core\AdRatingsComposite;
use AutoAction\AdRatings\Core\DataMemory;
use AutoAction\AdRatings\Enum\GroupsEnum;
use PHPUnit\Framework\TestCase;

class AdRatingsCompositeTest extends TestCase
{
    public function testConstructFalseComposite()
    {
        $data = new DataMemory(GroupsEnum::PHOTOS, 15);
        $composite = new AdRatingsComposite($data);
        $composite->execute();

        $calculate = $composite->getCalculateAdRatings();

        $this->assertSame(1.5, $calculate->getCalculation());
    }
}
